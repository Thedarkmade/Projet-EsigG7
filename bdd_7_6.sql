﻿CREATE TABLE bateau (
    matricule     VARCHAR(200) NOT NULL,
    Nom_bateau   VARCHAR(4000) NOT NULL,
    proprietaire   VARCHAR(2000) NOT NULL,
    longeur        INTEGER NOT NULL,
    largeur        INTEGER NOT NULL,
    hauteur        INTEGER NOT NULL,
    pquille        INTEGER NOT NULL,
    vitesse        INTEGER NOT NULL,
    deplacement       INTEGER NOT NULL,
    nationalité    VARCHAR(2000) NOT NULL,
    nbequipage     INTEGER NOT NULL,
    date_arr       DATE NOT NULL,
    date_dep       DATE NOT NULL,
    description    TEXT NOT NULL,
    id_image       VARCHAR(4000) ,
    id_pdf         VARCHAR(4000) 
);

ALTER TABLE bateau ADD CONSTRAINT bateau_pk PRIMARY KEY ( matricule );

CREATE TABLE hash (
    psalt   VARCHAR(4000) NOT NULL,
    ssalt   VARCHAR(4000) NOT NULL
);

CREATE TABLE login (
    login   VARCHAR(4000) NOT NULL,
    psw     VARCHAR(191) NOT NULL
);

ALTER TABLE login ADD CONSTRAINT login_pk PRIMARY KEY ( psw );

CREATE TABLE personne (
    login            VARCHAR (191) NOT NULL,
    nom              VARCHAR(4000) NOT NULL,
    prenom           VARCHAR(4000) NOT NULL,
    date_naissance   DATE NOT NULL,
    mail             VARCHAR(4000) NOT NULL,
    plvl             INTEGER NOT NULL
);

ALTER TABLE personne ADD CONSTRAINT personne_pk PRIMARY KEY ( login );

ALTER TABLE bateau
    ADD CONSTRAINT bateau_personne_fk FOREIGN KEY ( proprietaire )
        REFERENCES personne ( login )
            ON DELETE CASCADE;

ALTER TABLE login
    ADD CONSTRAINT login FOREIGN KEY ( login )
        REFERENCES personne ( login )
            ON DELETE CASCADE;
INSERT INTO `hash`(`psalt`, `ssalt`) VALUE ('g7Psalt','g7Ssalt')